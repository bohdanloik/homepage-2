import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterName = createFighterName(fighter);
    const fighterInfo = createFighterInfo(fighter);
  
    fighterElement.append(fighterImage, fighterName, fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterName(fighter) {
  const { name } = fighter;
  const fighterName = createElement({ 
    tagName: 'h2', 
    className: 'fighter-preview___name',
  });
  fighterName.innerText = name;

  return fighterName;
}

export function createFighterInfo(fighter) {
  const fighterInfoKeys = ['health', 'attack', 'defense'];
  const fighterInfoList = Object.entries(fighter).filter(item => fighterInfoKeys.includes(item[0]));

  const fighterInfoContainer = createElement({
    tagName: 'ul',
    className: 'fighter-preview__info',
  });

  const fighterInfoItems = fighterInfoList.map((infoItem => createFighterInfoItem(infoItem)));
  fighterInfoContainer.append(...fighterInfoItems);
  
  return fighterInfoContainer;
}

export function createFighterInfoItem(info) {
  const [name, value] = info;

  const fighterInfoItemContainer = createElement({
    tagName: 'li',
    className: 'fighter-preview__info-item',
  });

  const fighterInfoItemName = createElement({ 
    tagName: 'span', 
    className: 'fighter-preview___info-item-name',
  });
  fighterInfoItemName.innerText = name;

  const fighterInfoItemValue = createElement({ 
    tagName: 'span', 
    className: 'fighter-preview___info-item-value',
  });
  fighterInfoItemValue.innerText = value;

  fighterInfoItemContainer.append(fighterInfoItemName, fighterInfoItemValue);

  return fighterInfoItemContainer;
}
