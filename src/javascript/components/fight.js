import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const fighter = {
      ...firstFighter,
      currentHealth: firstFighter.health,
      healthIndicator: document.getElementById('left-fighter-indicator')
    };
    const fighter2 = {
      ...secondFighter,
      currentHealth: secondFighter.health,
      healthIndicator: document.getElementById('right-fighter-indicator')
    };
    let keys = new Set();

    document.addEventListener('keydown', (event) => {
      keys.add(event.code);
    
      switch(event.code) {
        case controls.PlayerOneAttack:
          if (!keys.has(controls.PlayerOneBlock) && !keys.has(controls.PlayerTwoBlock)) {
            attack(fighter, fighter2);
            if (checkHealth(fighter2)) {
              resolve(fighter);
            }
          }
          break;
        case controls.PlayerTwoAttack:
          if (!keys.has(controls.PlayerTwoBlock) && !keys.has(controls.PlayerOneBlock)) {
            attack(fighter2, fighter);
            if (checkHealth(fighter)) {
              resolve(fighter2);
            }
          }
          break;
      }
    });

    document.addEventListener('keyup', (event) => {
      keys.delete(event.code);
    })
  });
}

export function attack(attacker, defender) {
  const damage = getDamage(attacker, defender);
  defender.currentHealth -= damage;
  setHealthIndicator(defender);
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  return damage > 0 ? damage : 0;
}

export function getCriticalDamage(attacker) {
  const { attack } = attacker;
  const damage = attack * 2;

  return damage;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = getRandomArbitrary(1, 2);

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = getRandomArbitrary(1, 2);

  return defense * dodgeChance;
}

export function setHealthIndicator(fighter) {
  const { health, currentHealth, healthIndicator } = fighter;
  let percents = currentHealth * 100 / health;
  percents = percents > 0 ? percents : 0;
  healthIndicator.style.width = `${percents}%`;
}

export function checkHealth(fighter) {
  const { currentHealth } = fighter;

  return currentHealth <= 0 ? true : false;
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
